package de.hhn.hellohhn;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloHhnController {

    @GetMapping("/hellohhn")
    public String sayHello() {
        return "Hello hhn, how are you!";
    }

}
