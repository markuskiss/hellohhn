package de.hhn.hellohhn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellohhnApplication {

    public static void main(String[] args) {
        SpringApplication.run(HellohhnApplication.class, args);
    }

}
